# websocket 实战
>不是 demo，是真正的企业实战
> 
> 项目仅满足了当前业务场景，"抛砖引玉"

## 需求说明
用户通过平台观察 task，服务推送最新的 task 进展到用户端
存在场景：
1. 一个用户观察一个 task
2. 多个用户观察同一个 task

## 项目功能
1. websocket 通信
2. 支持安全认证
3. "消息"广播
4. 集群支持

## 工程说明
1. 启动类 ``` src/main/java/com/example/websocket/WebsocketApplication.java ```
2. 认证拦截 ``` src/main/java/com/example/websocket/interceptors/WsHandShakeInterceptor.java ```
3. 分发处理器 ``` src/main/java/com/example/websocket/ws/TaskWsHandler.java ```
4. 消息处理器 ``` src/main/java/com/example/websocket/ws/handler/AbstractMessageHandler.java ```

## 二开说明
分发处理器如果没有特别要求无需改动

仅根据业务需要修改 ` src/main/java/com/example/websocket/ws/handler/AbstractMessageHandler.java `

然后继承，实现真正的业务逻辑即可

## 集群说明
如果集群部署，后端消息不能直接发送，要通过 mq、redis、eventBus 类广播出去

然后由各个服务监听，然后调用 com.example.websocket.ws.TaskWsHandler.handlerAll(Map map, String id)

完成消息广播

#### 原因
websocket 不同于 session 可以通过 redis 做集群共享

必须强关联到具体的机器，因为底层走的 tcp 连接

广播必须要每个在线的服务都检查自己当前的"客户（建立了websocket连接的客户端）"是否需要该消息

## 问题
现在如果握手阶段，发现缺失参数。但是逻辑没有终止，而是到达真正建立连接之后，推送"异常消息"然后关闭连接

造成资源浪费

### Todo
+[ ] 认证失败直接返回
