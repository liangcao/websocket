package com.example.websocket;

import com.example.websocket.interceptors.WsHandShakeInterceptor;
import com.example.websocket.ws.TaskWsHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;


@EnableWebSocket
@EnableScheduling
@SpringBootApplication
public class WebsocketApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebsocketApplication.class, args);
    }

    @Bean
    public WebSocketConfigurer webSocketConfigurer(TaskWsHandler handler,
                                                   WsHandShakeInterceptor interceptor) {
        return registry -> registry
                /**
                 * ws/history?performId=xxx
                 * 通过后面的自定义参数，完成 session 订阅
                 */
                .addHandler(handler, "/ws/history")
                .addInterceptors(interceptor)
                .setAllowedOrigins("*");
    }

}
