package com.example.websocket.bean;

import java.net.URLDecoder;

/**
 * Tenant
 *
 * 租户信息
 */
public class Tenant {
    // 用户信息
    private String userId = "";
    private String userName = "";

    // 租户信息
    private String accountId = "";
    private String accountName = "";


    public String serialize2Json() {
        StringBuilder sb = new StringBuilder("{");

        sb.append("\"userId\":\"").append(userId).append("\",")
                .append("\"userName\":\"").append(userName).append("\",")
                .append("\"accountId\":\"").append(accountId).append("\",")
                .append("\"accountName\":\"").append(accountName).append("\",")
        ;

        sb.append("}");
        return sb.toString();
    }

    /**
     * 因为名字可能包含中文，需要decode
     * @param encodeStr
     * @return
     */
    private String decodeString(String encodeStr) {
        try {
//            return URLDecoder.decode(encodeStr, Charset.defaultCharset().name());
            // 如果使用了网关，根据网关编码调整
            return URLDecoder.decode(encodeStr, "UTF-8");
        } catch (Exception e) {
            return encodeStr;
        }
    }

    public Tenant() {
        //空参构造
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = decodeString(userName);
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = decodeString(accountName);
    }

}
