package com.example.websocket.constant;


public enum WsEventEnum {

    /**
     * 未知
     */
    UNKNOWN(0, "unknown"),
    /**
     * job view
     */
    VIEW(1, "view"),
    /**
     * eventLog
     */
    LOG(2, "log"),


    CLOSE(-1, "close");



    private int code;
    private String value;

    WsEventEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
