package com.example.websocket.controller;

import com.example.websocket.ws.TaskWsHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * MockEventController
 *
 * @author liam
 * @date 2023/1/16
 */
@RestController
@RequestMapping("/test")
public class MockEventController {
    @Resource
    TaskWsHandler handler;

    @GetMapping("publish/{performId}")
    public String publishEvent(@PathVariable String performId) {

        /**
         * @see com.example.websocket.ws.handler.EventLogMessageHandler#doHandle(Map, WebSocketSession)
         */
        Map<String, Object> eventLogMsg = new HashMap<>();
        eventLogMsg.put("event", "log");
        eventLogMsg.put("taskId", "xxx");

        handler.handlerAll(eventLogMsg, performId);

        return "publish event,success";
    }
}
