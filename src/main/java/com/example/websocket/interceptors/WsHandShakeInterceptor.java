package com.example.websocket.interceptors;

import com.example.websocket.bean.Tenant;
import com.example.websocket.utils.UUIDUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.server.HandshakeInterceptor;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * WsHandShakeInterceptor
 *
 */
@Component
public class WsHandShakeInterceptor implements HandshakeInterceptor {
    private static final String KEY = "unique_key";
    public static final String SESSION_KEY = "perform_id";
    public static final String ERR_KEY = "error";

    private static final String ACCOUNT_NAME = "accountName";
    private static final String ACCOUNT_ID = "accountId";
    private static final String USER_ID = "userId";
    private static final String USER_NAME = "userName";
    private static final ConcurrentHashMap<String, Tenant> tenants = new ConcurrentHashMap<>(128);

    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response,
                                   WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
        if (request instanceof ServletServerHttpRequest) {
            HttpServletRequest servletRequest = ((ServletServerHttpRequest) request).getServletRequest();
            String performId = servletRequest.getParameter("performId");

            Tenant tenant = getTenant(servletRequest);

            if (StringUtils.isAnyBlank(performId, tenant.getAccountId(), tenant.getUserId())) {
                attributes.put(ERR_KEY, "URl param 'performId' or Header value 'accountId/userId' missing");
            } else {
                // concurrentHashMap key and value can not be 'null'
                attributes.put(SESSION_KEY, performId);
            }

            String uniqueKey = UUIDUtil.build();

            tenants.put(uniqueKey, tenant);
            attributes.put(KEY, uniqueKey);

            return true;
        }
        return false;
    }

    @Override
    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception exception) {
    }

    public static Tenant getTenant(WebSocketSession session) {
        return tenants.get(session.getAttributes().get(KEY));
    }

    public static void removeTenant(WebSocketSession session) {
        tenants.remove(session.getAttributes().get(KEY));
    }

    public static Tenant getTenant(HttpServletRequest request) {
        Tenant tenant = new Tenant();
        tenant.setAccountName(request.getHeader(ACCOUNT_NAME));
        tenant.setAccountId(request.getHeader(ACCOUNT_ID));

        tenant.setUserId(request.getHeader(USER_ID));
        tenant.setUserName(request.getHeader(USER_NAME));

        return tenant;
    }
}
