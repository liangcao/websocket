package com.example.websocket.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

public class JsonUtil {
    private static final Logger log = LoggerFactory.getLogger(JsonUtil.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES,true);
    }

    /**
     * 对象转化字符串
     *
     * @param object 对象
     * @return String
     */
    public static String toJSONString(Object object) {
        try {
            if (object == null) {
                return null;
            }
            if (object instanceof String) {
                return (String) object;
            }
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error("Object conversion String failed:{}", ExceptionUtils.getStackTrace(e));
        }
        return null;
    }


    /**
     * Json字符串转为Map对象
     *
     * @param json Json字符串
     * @return Map对象
     * @throws IOException 异常信息
     */
    public static Map<String, Object> parseObjectThrow(String json) throws IOException {
        return mapper.readValue(json, new TypeReference<Map<String, Object>>() {
        });
    }


}
