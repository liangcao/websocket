package com.example.websocket.utils;

import java.util.UUID;

public class UUIDUtil {
    private static final char[] MATRIX = {
            '0' , '1' , '2' , '3' , '4' , '5' ,
            '6' , '7' , '8' , '9' , 'a' , 'b' ,
            'c' , 'd' , 'e' , 'f' , 'g' , 'h' ,
            'i' , 'j' , 'k' , 'l' , 'm' , 'n' ,
            'o' , 'p' , 'q' , 'r' , 's' , 't' ,
            'u' , 'v' , 'w' , 'x' , 'y' , 'z'
    };

    public static String build() {
        UUID uuid = UUID.randomUUID();
        long leastSigBits = uuid.getLeastSignificantBits();
        long mostSigBits = uuid.getMostSignificantBits();
        char[] str_uid = new char[32];
        digits(str_uid,mostSigBits >> 32, 8, 8);
        digits(str_uid, mostSigBits >> 16, 4, 12);
        digits(str_uid, mostSigBits, 4, 16);
        digits(str_uid,leastSigBits >> 48, 4, 20);
        digits(str_uid, leastSigBits, 12, 32);
        return new String(str_uid);

    }

    private static void digits(char[] uuid, long val, int digits, int offset) {
        long hi = 1L << (digits << 2);
        val = hi | (val & (hi -1));
        do {
            --digits;
            uuid[--offset] = MATRIX[((int) val) & 15];
            val >>>= 4;
        } while ( val!=0 && digits > 0);
    }
}
