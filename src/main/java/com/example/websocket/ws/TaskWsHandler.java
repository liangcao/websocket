package com.example.websocket.ws;

import com.example.websocket.interceptors.WsHandShakeInterceptor;
import com.example.websocket.utils.JsonUtil;
import com.example.websocket.ws.handler.AbstractMessageHandler;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.TimeUnit;

@Component
public class TaskWsHandler extends TextWebSocketHandler {

    private static final Logger log = LoggerFactory.getLogger(TaskWsHandler.class);
    private static final String PING = "ping";
    private static final String PONG = "pong";

    private AbstractMessageHandler handler;

    /**
     * performId: [sessionId1, sessionId2]
     */
    private static final ConcurrentHashMap<String, Set<String>> HID_SID = new ConcurrentHashMap<>();
    /**
     * session_id: YES
     */
    private static final ConcurrentHashMap<String, Byte> ON_CREATING = new ConcurrentHashMap<>();
    private static final Byte CREATING_PLACEHOLDER = 1;
    private static final LoadingCache<String, WebSocketSession> CACHES = CacheBuilder.newBuilder()
            .expireAfterAccess(59, TimeUnit.SECONDS)
            .initialCapacity(128)
            .removalListener((notification -> {
                WebSocketSession session = (WebSocketSession) notification.getValue();
                if (session.isOpen()) {
                    clean(session);
                    try {
                        session.close(CloseStatus.SERVICE_OVERLOAD);
                    } catch (IOException e) {
                        log.error("close websocket error: {}", ExceptionUtils.getStackTrace(e));
                    }
                }
            }))
            .build(new CacheLoader<String, WebSocketSession>() {
                       @Override
                       public WebSocketSession load(String key) throws Exception {
                           return null;
                       }
                   }
            );



    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        String err = (String)session.getAttributes().get(WsHandShakeInterceptor.ERR_KEY);
        if (null!=err) {
            session.sendMessage(new TextMessage(err));
            session.close(CloseStatus.NOT_ACCEPTABLE.withReason(err));
            return;
        }

        CACHES.put(session.getId(), session);
        String key = getPerformId(session);
        ON_CREATING.put(key, CREATING_PLACEHOLDER);

        HID_SID.putIfAbsent(key , new HashSet<>());
        HID_SID.get(key).add(session.getId());
        WAIT_SENDING.put(session.getId(), new ConcurrentLinkedDeque<>());

        ON_CREATING.remove(key);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        log.debug("ws msg: {}", JsonUtil.toJSONString(message));

        String msgBody = message.getPayload();
        if (PING.equalsIgnoreCase(msgBody)) {
            send(CACHES.getIfPresent(session.getId()), PONG);
            return;
        }
        try {
            Map<String, Object> msg = JsonUtil.parseObjectThrow(msgBody);

            handler.handle(msg, getSessionFromCache(session));

        } catch (Exception e) {
            log.error("handle message error:{}", ExceptionUtils.getStackTrace(e));
        }
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        super.handleTransportError(session, exception);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        clean(session);
    }

    private static String getPerformId(WebSocketSession session) {
        return session.getAttributes().get(WsHandShakeInterceptor.SESSION_KEY).toString();
    }

    public WebSocketSession getSessionFromCache(WebSocketSession session) {
        WebSocketSession cs = CACHES.getIfPresent(session.getId());
        if (null==cs) {
            log.error("get session from cache failed,sessionId: {}, and recache", session.getId());
            CACHES.put(session.getId(), session);
            return session;
        }
        return cs;
    }


    /**
     * clean cache
     * @param session
     */
    private static void clean(WebSocketSession session) {

        String key = getPerformId(session);
        WsHandShakeInterceptor.removeTenant(session);
        Set<String> sids = HID_SID.getOrDefault(key, new HashSet<>());
        sids.remove(session.getId());
        if (!ON_CREATING.contains(key) && sids.size()==0) {
            HID_SID.remove(key);
        }
        WAIT_SENDING.remove(session.getId());
//        CACHES.invalidate(getSessionKey(session));
    }

    public Set<WebSocketSession> getAllSessionByPerformId(String performId) {
        Set<String> sids = HID_SID.get(performId);
        HashSet<WebSocketSession> sessions = new HashSet<>();
        for (String sid : sids) {
            sessions.add(CACHES.getIfPresent(sid));
        }
        return sessions;
    }

    public void handlerAll(Map<String, Object> msg, String performId) {
        if (StringUtils.isBlank(performId)) {
            return;
        }
        Set<String> sids = HID_SID.getOrDefault(performId, Collections.emptySet());
        log.debug("listeners:{}, for performId: {}", JsonUtil.toJSONString(sids), performId);
        for (String sid : sids) {
            handler.handle(msg, CACHES.getIfPresent(sid));
        }
    }

    private static final ConcurrentHashMap<String, ConcurrentLinkedDeque<String>> WAIT_SENDING = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, Byte> ON_SENDING = new ConcurrentHashMap<>();
    public static void send(WebSocketSession session, String msg) {
        try {
            if (null==session || !session.isOpen()) {
                log.error("session status is error: {}, msg: {} drop", JsonUtil.toJSONString(session), msg);
                return;
            }

            if (PONG.equals(msg)) {
                WAIT_SENDING.get(session.getId()).addFirst(msg);
            } else {
                WAIT_SENDING.get(session.getId()).offer(msg);

            }

            log.debug("will send msg: {} ,to session: {}", msg, session.getId());

            doSend(session);
        } catch (Exception e) {
            log.error("send msg error: {}", ExceptionUtils.getStackTrace(e));
        }
    }

    private static void doSend(WebSocketSession session) {
        if (null==ON_SENDING.putIfAbsent(session.getId(), CREATING_PLACEHOLDER)) {
            if (session.isOpen()) {
                String s;
                while ((s = WAIT_SENDING.get(session.getId()).poll()) != null) {
                    try {
                        session.sendMessage(new TextMessage(s));
                    } catch (Exception e) {
                        // maybe current thread waiting blocking io, terminal this time send
                        WAIT_SENDING.get(session.getId()).addFirst(s);
                        log.error("terminal send,and wait next time. msg:{}, error: {}", s, ExceptionUtils.getStackTrace(e));
                        break;
                    }
                }
            } else {
                log.error("session: {} has been closed", session.getId());
            }
            ON_SENDING.remove(session.getId());
        }
    }

    public void addHandler(AbstractMessageHandler handler) {
        synchronized (this) {
            if (null==this.handler) {
                this.handler = handler;
            } else {
                this.handler = this.handler.addHeadChain(handler);
            }
        }
    }


    /**
     * auto clean resource
     * every 1s * ?
     */
    @Scheduled(fixedRate = 1000 * 100)
    private void autoClean() {
        CACHES.cleanUp();
    }

}
