package com.example.websocket.ws.handler;

import com.example.websocket.bean.Tenant;
import com.example.websocket.constant.WsEventEnum;
import com.example.websocket.interceptors.WsHandShakeInterceptor;
import com.example.websocket.utils.JsonUtil;
import com.example.websocket.ws.TaskWsHandler;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractMessageHandler<T> {

    private static final Logger log = LoggerFactory.getLogger(AbstractMessageHandler.class);
    @Autowired
    TaskWsHandler historyTaskWsHandler;

    private static final String EVENT = "event";

    protected AbstractMessageHandler nextHandler;
    protected WsEventEnum event;

    protected boolean preHandle(String type) {
        return false;
    }

    protected abstract void doHandle(Map<String, Object> msg, WebSocketSession session);

    public void handle(Map<String, Object> msg, WebSocketSession session) {
        log.debug("param msg: {}", JsonUtil.toJSONString(msg));

        if (null == session ) {
            log.error("session is null, msg:{}", JsonUtil.toJSONString(msg));
            return;
        }

        if (null==msg || msg.isEmpty()) {
            sendErr(session,"params is empty");
            return;
        }

        if (StringUtils.isBlank(String.valueOf(msg.get(EVENT)))) {
            sendErr(session,"param:[event] is needed");
            return;
        }

        AbstractMessageHandler currentHandler = this;
        do {
            if (currentHandler.preHandle(String.valueOf(msg.get(EVENT)))) {
                currentHandler.doHandle(msg, session);
                break;
            }
        } while ((currentHandler=currentHandler.nextHandler)!=null);

        if (null==currentHandler) {
            log.warn("no handler process msg: {}", JsonUtil.toJSONString(msg));
        }
    }


    protected void send(WebSocketSession session, T msg, WsEventEnum eventEnum) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("data", msg);
            map.put("type", eventEnum.getValue());
            map.put("code", "success");
            map.put("msg", "OK");

            TaskWsHandler.send(session, JsonUtil.toJSONString(map));
        } catch (Exception e) {
            log.error("send msg error: {}", ExceptionUtils.getStackTrace(e));
        }
    }

    protected void sendErr(WebSocketSession session, String msg) {
        try {
            Map<String, String> map = new HashMap<>();
            map.put("code", "error");
            map.put("msg", msg);

            TaskWsHandler.send(session, JsonUtil.toJSONString(map));
        } catch (Exception e) {
            log.error("send msg error: {}", ExceptionUtils.getStackTrace(e));
        }
    }


    public AbstractMessageHandler addChain(AbstractMessageHandler nextHandler) {
        this.nextHandler = nextHandler;
        return nextHandler;
    }

    public AbstractMessageHandler addHeadChain(AbstractMessageHandler nextHandler) {
        nextHandler.nextHandler = this;
        return nextHandler;
    }

    protected Tenant getTenant(WebSocketSession session) {
        return WsHandShakeInterceptor.getTenant(session);
    }

    protected String getStringFromObj(Object obj) {
        if (null==obj) {
            return null;
        }

        if (obj instanceof String) {
            String str = StringUtils.trimToNull(String.valueOf(obj));
            if ("null".equals(str)) {
                return null;
            }
            return str;
        }

        return JsonUtil.toJSONString(obj);
    }

    @PostConstruct
    private void init() {
        historyTaskWsHandler.addHandler(this);
    }

}
