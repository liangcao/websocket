package com.example.websocket.ws.handler;

import com.example.websocket.constant.WsEventEnum;
import com.example.websocket.utils.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * EventLogMessageHandler
 *
 */
@Component
public class EventLogMessageHandler extends AbstractMessageHandler<List> {
    private static final Logger log = LoggerFactory.getLogger(EventLogMessageHandler.class);

    public EventLogMessageHandler() {
        // 初始化当前 handler 的事件
        this.event = WsEventEnum.LOG;
    }

    @Override
    protected boolean preHandle(String type) {
        return event.getValue().equalsIgnoreCase(type);
    }

    /**
     * {
     * "event": "log",
     * "jobId": "xxx",
     * "taskId": "xxx"
     * }
     * @param msg
     */
    @Override
    protected void doHandle(Map<String, Object> msg, WebSocketSession session) {

        /**
         * 获取租户信息
         * 方便后续业务逻辑
         */
//        Tenant tenant = getTenant(session);

        /**
         * msg 参数校验
         */
        if (StringUtils.isBlank(getStringFromObj(msg.get("taskId")))) {
            sendErr(session, "param[taskId] is needed");
            return;
        }

        try {
            log.debug("input param: {}", JsonUtil.toJSONString(msg));

            // 根据业务获取真实数据，此处mock
            List<String> result = mockData();

            send(session, result, event);
        } catch (Exception e ) {
            log.error("error, params: {}, error: {}, errorMsg: {}",
                    JsonUtil.toJSONString(msg), ExceptionUtils.getStackTrace(e), e.getMessage());

            sendErr(session,"error["+event.getValue()+"]");
        }

    }


    private List<String> mockData() {
        AtomicInteger sequence = new AtomicInteger(1);
        return Stream.generate(()-> sequence.getAndIncrement()).limit(new Random().nextInt(20)+10)
                .map(seq-> String.format("[%s]-[%s] mock log",
                        LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), seq))
                .collect(Collectors.toList());
    }
}
