package com.example.websocket.ws.handler;

import com.example.websocket.constant.WsEventEnum;
import com.example.websocket.utils.JsonUtil;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class ViewMessageHandler extends AbstractMessageHandler<List<String>> {
//public class ViewMessageHandler extends AbstractMessageHandler<List<XXBean>> {

    private static final Logger log = LoggerFactory.getLogger(ViewMessageHandler.class);

    public ViewMessageHandler() {
        this.event = WsEventEnum.VIEW;
    }

    @Override
    protected boolean preHandle(String type) {
        return event.getValue().equalsIgnoreCase(type);
    }

    /**
     * {
     * "event":"view",
     * "taskIds":"[xxx]"
     * }
     * @param msg
     */
    @Override
    protected void doHandle(Map<String, Object> msg, WebSocketSession session) {

        List<String> taskIds = (List<String>) msg.get("taskIds");
        if (null==taskIds || taskIds.isEmpty()) {
            sendErr(session,"param:[taskIds] is needed");
            return;
        }
//        Tenant tenant = getTenant(session);

        try {
            log.debug("input param: {}, taskIds: {}",
                    JsonUtil.toJSONString(msg), JsonUtil.toJSONString(taskIds));

            List<String> results = new ArrayList<>();


            send(session, results, event);
        } catch (Exception e) {
            log.error("error, params: {}, error: {}, errorMsg: {}",
                    JsonUtil.toJSONString(msg), ExceptionUtils.getStackTrace(e), e.getMessage());

            sendErr(session,"error["+event.getValue()+"]");
        }

    }

}
